## OPENSTACK VARIABLES
OS_PROJECT_ID ?=geral;system-team;admin
OS_AUTH_URL ?= http://192.168.93.215:5000/v3/

V ?=
PRIVATE_VARS ?= extra_vars.yml
INVENTORY_FILE ?= ./hosts
NODES ?= all
EXTRA_VARS ?= extra_vars.yml
COLLECTIONS_PATHS ?= ./collections

SLACK_API_URL ?= ******************
SLACK_API_URL_MVP ?= ******************
SLACK_CHANNEL ?= prometheus-alerts
SLACK_CHANNEL_MVP ?= proj-mvp-messages
PROMETHEUS_ALERTMANAGER_URL ?= http://192.168.93.26:9093
PROMETHEUS_URL ?= https://alerts.engageska-portugal.pt
PROM_CONFIGS_PATH ?= .

ARCHIVER_PASSWORD ?= "mandatory"
GRAFANA_PASSWORD ?= "mandatory"
TANGODB_PASSWORD ?= "mandatory"
KUBECONFIG ?= "mandatory"
GITLAB_TOKEN ?= "mandatory"

# AzureAD vars
AZUREAD_CLIENT_ID ?=
AZUREAD_CLIENT_SECRET ?=
AZUREAD_TENANT_ID ?=

.PHONY: vars ansible_install help
.DEFAULT_GOAL := help
PROMETHEUS_EXTRAVARS ?=

# define private rules
-include PrivateRules.mak

ANSIBLE_COLLECTIONS_PATHS ?= $(COLLECTIONS_PATHS):~/.ansible/collections:/usr/share/ansible/collections
DOCKER_PLAYBOOKS ?= $(COLLECTIONS_PATHS)/ansible_collections/ska_cicd/docker_base/playbooks
DOCKER_ROLES ?= $(COLLECTIONS_PATHS)/collections/ansible_collections/ska_cicd/docker_base/roles/docker

vars:  ## Variables
	@echo "Current variable settings:"
	@echo "PRIVATE_VARS=$(PRIVATE_VARS)"
	@echo "CLUSTER_KEYPAIR=$(CLUSTER_KEYPAIR)"
	@echo "INVENTORY_FILE=$(INVENTORY_FILE)"
	@echo "EXTRA_VARS=$(EXTRA_VARS)"
	@echo "ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS)"
	@echo "STACK_CLUSTER_PLAYBOOKS=$(STACK_CLUSTER_PLAYBOOKS)"
	@echo "DOCKER_PLAYBOOKS=$(DOCKER_PLAYBOOKS)"

## PREREQUISITES FIRST

test:
	@echo $(DOCKER_PLAYBOOKS)
	@echo
	@cat $(DOCKER_PLAYBOOKS)/docker.yml

uninstall:  # Uninstall collections
	rm -rf $(COLLECTIONS_PATHS)/ansible_collections/*

install:  ## Install dependent ansible collections
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-galaxy collection install \
	-r requirements.yml -p ./collections

reinstall: uninstall install ## reinstall collections

build_docker:  ## apply the docker roles
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
 	ansible-playbook -i $(INVENTORY_FILE) $(DOCKER_PLAYBOOKS)/docker.yml $(PROMETHEUS_EXTRAVARS) 

## SERVER AND NODE EXPORTER INSTALLATIONS
all: build

lint: ## Lint playbooks
	yamllint -d "{extends: relaxed, rules: {line-length: {max: 350}}}" \
			deploy_docker_exporter.yml  deploy_node_exporter.yml  deploy_prometheus.yml  export_runners.yml  roles/*
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-lint --exclude=roles/prometheus/files \
	 deploy_docker_exporter.yml  deploy_node_exporter.yml \
	 deploy_prometheus.yml  export_runners.yml \
	  roles/*  > ansible-lint-results.txt; \
	cat ansible-lint-results.txt
	flake8 --exclude roles/prometheus/files/openstack roles/*

build: ## Install Prometheus on new Openstack instance
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook deploy_prometheus.yml \
		--extra-vars "mode='server' slack_api_url='$(SLACK_API_URL)' slack_api_url_mvp='$(SLACK_API_URL_MVP)'" \
		--extra-vars="azuread_client_id='$(AZUREAD_CLIENT_ID)' azuread_client_secret='$(AZUREAD_CLIENT_SECRET)' azuread_tenant_id='$(AZUREAD_TENANT_ID)'" \
		-e "slack_channel='$(SLACK_CHANNEL)'" \
		-e "slack_channel_mvp='$(SLACK_CHANNEL_MVP)'" \
		-e "prometheus_alertmanager_url='$(PROMETHEUS_ALERTMANAGER_URL)'" \
		-e "project_name='$(OS_PROJECT_NAME)' project_id='$(OS_PROJECT_ID)' auth_url='$(OS_AUTH_URL)'" \
		-e "archiver_password='$(ARCHIVER_PASSWORD)' grafana_admin_password='$(GRAFANA_PASSWORD)' tangodb_password='$(TANGODB_PASSWORD)' kubeconfig='$(KUBECONFIG)'" \
		-e "username='$(OS_USERNAME)' password='$(OS_PASSWORD)'" \
		-e "prometheus_url='$(PROMETHEUS_URL)'" $(PROMETHEUS_EXTRAVARS) \
		-e "prometheus_gitlab_ci_pipelines_exporter_token=$(GITLAB_TOKEN)" \
		-i hosts \
		-e @$(PROM_CONFIGS_PATH)/prometheus_node_metric_relabel_configs.yaml \
		-e 'ansible_python_interpreter=/usr/bin/python3' $(V)

server: ## Install Prometheus Server
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook deploy_prometheus.yml \
		--extra-vars "mode='server' slack_api_url='$(SLACK_API_URL)' slack_api_url_mvp='$(SLACK_API_URL_MVP)'" \
		--extra-vars="azuread_client_id='$(AZUREAD_CLIENT_ID)' azuread_client_secret='$(AZUREAD_CLIENT_SECRET)' azuread_tenant_id='$(AZUREAD_TENANT_ID)'" \
		-e "slack_channel='$(SLACK_CHANNEL)'" \
		-e "slack_channel_mvp='$(SLACK_CHANNEL_MVP)'" \
		-e "prometheus_alertmanager_url='$(PROMETHEUS_ALERTMANAGER_URL)'" \
		-e "project_name='$(OS_PROJECT_NAME)' project_id='$(OS_PROJECT_ID)' auth_url='$(OS_AUTH_URL)'" \
		-e "archiver_password='$(ARCHIVER_PASSWORD)' grafana_admin_password='$(GRAFANA_PASSWORD)' tangodb_password='$(TANGODB_PASSWORD)' kubeconfig='$(KUBECONFIG)'" \
		-e "username='$(OS_USERNAME)' password='$(OS_PASSWORD)'" \
		-e "prometheus_url='$(PROMETHEUS_URL)'" $(PROMETHEUS_EXTRAVARS) \
		-e "prometheus_gitlab_ci_pipelines_exporter_token=$(GITLAB_TOKEN)" \
		-i hosts \
		-e @$(PROM_CONFIGS_PATH)/prometheus_node_metric_relabel_configs.yaml \
		-e 'ansible_python_interpreter=/usr/bin/python3' $(V)

thanos: ## Install Thanos query and query front-end
	ANSIBLE_COLLECTIONS_PATHS=$(ANSIBLE_COLLECTIONS_PATHS) \
	ANSIBLE_COLLECTIONS_PATH=$(ANSIBLE_COLLECTIONS_PATHS) \
	ansible-playbook deploy_prometheus.yml \
		--extra-vars "mode='thanos'" \
		-i hosts \
		-e 'ansible_python_interpreter=/usr/bin/python3' $(V)

node-exporter: ## Install Prometheus node exporter - pass INVENTORY_FILE and NODES
	ansible-playbook deploy_node_exporter.yml -i $(INVENTORY_FILE) \
	-e 'ansible_python_interpreter=/usr/bin/python3' \
	-e @$(EXTRA_VARS) \
	--limit $(NODES)

update_metadata: ## Update OpenStack metadata for node_exporters - pass INVENTORY_FILE
	ansible -i ./inventory_prometheus all -b -m copy -a 'src=$(INVENTORY_FILE) dest=/tmp/all_inventory'
	@ansible -i ./inventory_prometheus all -b -m shell -a 'export project_name=$(OS_PROJECT_NAME) project_id=$(OS_PROJECT_ID) auth_url=$(OS_AUTH_URL) username=$(OS_USERNAME) password=$(OS_PASSWORD)	os_region_name=RegionOne os_interface=public os_project_id=$(OS_PROJECT_ID)	os_user_domain_name=default	os_identity_api_version=3 && python3 /usr/local/bin/prom_helper.py -u /tmp/all_inventory'

update_scrapers: ## Force update of scrapers
	ansible -i ./inventory_prometheus all -b -m shell -a 'cd /etc/prometheus && python3 /usr/local/bin/prom_helper.py -g && cp prometheus_node_metric_relabel_configs.yaml /usr/src/deploy-prometheus/'

help:  ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
