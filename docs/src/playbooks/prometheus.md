# Prometheus playbook
The prometheus playbook is now composed by the following tasks:
* server.yml: configure (prometheus configuration+recording rules+alerting rules) and start the prometheus server as docker container
* alert_manager.yml: configure and start the alert manager as docker container
* node_exporter.yml: this was the only task that was present before and it has simplified with the installation of the debian package (no need to install the service the package installs it)
* blackbox_exporter.yml: pull and start the black box exporter
* main.yml: depending on the mode (server, executor and all) it includes the other tasks.

The variable file include an example configuration made for testing purpose which includes: 
* various scrape configs (a scrape config represent an endpoint which usually corresponding to a single process where prometheus collects information). Scrape configs are grouped by job, that is a collection of instances with the same purpose. 
* alert rules, recording rules and all the necessary information that goes into the prometheus server. 

## Usage
Call the playbook with the following command to install the prometheus server (which includes the db, the alert manager, the black box exporter and grafana): 

```
    ansible-playbook deploy_prometheus.yml --extra-vars "mode='server'" \
           -i hosts \
           -e 'ansible_python_interpreter=/usr/bin/python3' \
           -e @/path/to/prometheus_node_metric_relabel_configs.yaml
```

Call the playbook with the following command to install the node-exporter: 

```
    ansible-playbook deploy_prometheus.yml --extra-vars "mode='exporter'" \
           -i hosts \
           -e 'ansible_python_interpreter=/usr/bin/python3'
```

Call the playbook with the following command to install both server and exporter: 

```
    ansible-playbook deploy_prometheus.yml -i hosts \
           -e 'ansible_python_interpreter=/usr/bin/python3' \
           -e @/path/to/prometheus_node_metric_relabel_configs.yaml
```

Remember to check the variable file available in roles/prometheus/vars before calling the playbook. 

The file `/path/to/prometheus_node_metric_relabel_configs.yaml` is generated by `openstack/prom_helper.py`, and contains a list of `relabel_configs` to set the instance names of the metrics to the actual OpenStack server name.
