# SKA Prometheus Deployment

Deploying a Prometheus service for monitoring resources

# Quick Start

* Clone the repo:
```
git clone https://gitlab.com/ska-telescope/sdi/deploy-prometheus.git && cd deploy-prometheus
```

For working with Ansible, you need to install it and set up a connection (ssh if it's not localhost) to the machine(s) where the playbooks will run. Most of the `make` targets merely call one or more Ansible playbooks, so you need to install Ansible to use it too.

* Install Pip, Ansible, Podman and Make:
```
sudo apt-get update && sudo apt-get upgrade -y
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py # if you don't have pip installed
sudo python3 get-pip.py
sudo python3 -m pip install ansible
sudo apt-get -y install podman
sudo apt install make

```

* Edit the file "hosts" with the address(es) to want to manage (if not localhost).

* Add the ssh key to the managed hosts (if not localhost).

## Collections & Docker

The docker role is installed from the SKA Ansible collections by calling

```
    make install
```
or to upgrade it, calling
```
    make reinstall
```
to install docker, you can now call
```
    make build_docker
```

## Playbooks
For installation of Prometheus things one playbook that can be called with different tags for different behaviours. Use (and add to) the make targets for deploying different parts of the Prometheus stack:

```
    make server
```
will install the Prometheus server.

```
    make node-exporter INVENTORY_FILE=../cluster-k8s/inventory_k8s_system_core NODES=k8s-syscore-worker-10
```
will install the Node Exporter on a targeted node from a specific inventory file.

```
    make update_metadata INVENTORY_FILE=../cluster-k8s/inventory_k8s_system_core
```
Forces the update of OpenStack metadata of newly deployed nodes found in a specific inventory file

```
    make update_scrapers
```
Forces the update of the Prometheus scraper config (this is automatically updated hourly anyway)

For the above targets to work, the OpenStack environment variables need to be set in the `PrivateRules.mak` file or in your environment:
```
OS_PROJECT_NAME=system-team
OS_AUTH_URL=http://192.168.93.215:5000/v3/
OS_USERNAME=<user>
OS_PASSWORD=<password>
OS_PROJECT_ID=988f3e60e7834335b3187512411d9072
```

When deploying the Prometheus server, the following variables are required to be set in the `PrivateRules.mak` file or in your environment to enable AzureAD authentication on the Grafana deployment:
```
AZUREAD_CLIENT_ID=<azuread client id>
AZUREAD_CLIENT_SECRET=<azuread client secret>
AZUREAD_TENANT_ID=<azuread tenant id>
```

The following variables are used to configure the slack notifications:
- SLACK_API_URL: the default webhook URL for all alerts.
- SLACK_API_URL_MVP: the webhook URL associated with the proj-mvp channel.

The private variables that need to be set for the `make prometheus-all` target, that have a `OS_` prefix, are well described in the README for the [Cluster-k8s repo](https://gitlab.com/ska-telescope/sdi/cluster-k8s#setting-your-openstack-environment).
